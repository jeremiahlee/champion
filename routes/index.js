/*
 * GET home page.
 */

exports.index = function(req, res) {
	res.render('index', { title: 'Express' });
	console.log(req.session);
};

exports.twitter_auth = function(req, res) {
	
	
};

exports.twitter_share = function(req, res) {
	req.session.foo = 'bar';
	console.log(req.session);
	console.log('twitter_share', req.session.passport);
	// req.session.passport.user.token
	// req.session.passport.user.tokenSecret
	
	// Display a form, submit it via post
	res.render('share', { title: 'Express', awesm_url:'http://panelpicker.sxsw.com/vote/4740', awesm_id: '' });

	// Create an Awe.sm link
	// https://api.awe.sm/url.json?v=3&url=http://developers.awe.sm&key=5c8b1a212434c2153c2f2c2f2c765a36140add243bf6eae876345f8fd11045d9&tool=mKU7uN&channel=facebook-post
	//	tool = AehFJd
	//	campaign_name = "SXSWi 2013"
	//	campaign = "sxswi-2013"
	//	url = "http://panelpicker.sxsw.com/vote/4740"
	//	channel = "twitter"
	//	key = 
	//		jeremiah-demo: 628a8225a26417deb57bc3fa95d0a49be71b94797e94e66e140ed874b0e5cca4
	//		jeremiah: e3a202c0151fabe207a3cc99ccf3b6bd8a2a1d2eefb5bd72899986acda1080e6
	// Display it
};

exports.twitter_publish = function(req, res) {
	console.log('----- Start Publish -----');

	// Create Awe.sm share
	var request = require('request');
	request(
		{
			method: 'POST',
			uri: 'http://api.awe.sm/url/retweet.json',
			form: true,
			qs: {
				v: '3',
				key: '628a8225a26417deb57bc3fa95d0a49be71b94797e94e66e140ed874b0e5cca4',
				tool: 'AehFJd',
				channel: 'twitter',
				campaign_name: 'SXSWi 2013',
				campaign: 'sxswi-2013',
				text: req.body.text
			}
		},
		function (error, response, body) {
			if (error) {
				console.log('Awe.sm API Error: retweet.json');
				console.log('> Text: ', req.body.text);
				console.log('> Response: ', response);
				console.log('> Body: ', body);
				console.log('---------');
				
				// TODO: Render and die
			}
			console.log('post to twitter');
			if (!error && response.statusCode == 200) {
				var awesmResponse = JSON.parse(body);
				
				// Post to Twitter
				var twitter = require('ntwitter');	
				twit = new twitter({
					consumer_key: '4zb5zQ28nAsi93GYPWgHJw',
					consumer_secret: '73Qn3DQem1SS9jY5VFHB47OGAmcC5LG6SA1AsdkwnY',
					access_token_key: req.session.passport.user.token,
					access_token_secret: req.session.passport.user.tokenSecret
				});
				
				twit.verifyCredentials(function (err, data) {
					if (err) {
						console.log('Twitter API Error: verifyCredentials');
						console.log(data);
						console.log('---------');
						
						// TODO: Render and die
					}
				})
				.updateStatus(awesmResponse.awesm_text,
					function (err, data) {
						console.log('posted to twitter');
						if (err) {
							console.log('Twitter API Error: updateStatus');
							console.log(data);
							console.log('---------');
							
							// TODO: Render and die
						}
						
						// Update Awe.sm share
						var shareTime = new Date(data.created_at).getTime() / 1000;
						console.log('update awesm url', shareTime);
						request(
							{
								method: 'POST',
								uri: 'http://api.awe.sm/url/update/' + awesmResponse.awesm_urls[0].awesm_id + '.json',
								form: true,
								qs: {
									v: '3',
									key: '628a8225a26417deb57bc3fa95d0a49be71b94797e94e66e140ed874b0e5cca4',
									tool: 'AehFJd',
									service_userid: 'twitter:' + data.user.id_str,
									service_postid: 'twitter:' + data.id_str,
									service_postid_reach: data.user.followers_count,
									service_postid_shared_at: data.created_at,
									notes: data.text
								}
							},
							function (error, response, body) {
								if (error) {
									console.log('Awe.sm API Error: update.json');
									console.log('> Text: ', req.body.text);
									console.log('> Response: ', response);
									console.log('> Body: ', body);
									console.log('---------');
									
									// TODO: Render and die
								}
								
								console.log('update done ', response.statusCode, body);
								console.log({
									service_userid: 'twitter:' + data.user.id_str,
									service_postid: 'twitter:' + data.id_str,
									service_postid_reach: data.user.followers_count,
									service_postid_shared_at: data.created_at,
									notes: data.text
								});
								
								if (!error && response.statusCode == 200) {
									var awesmUpdateResponse = JSON.parse(body);
									console.log('Done!', awesmUpdateResponse);
									res.render('shared', { title: 'Done!', awesm_url:'', awesm_id: '' });
								}
							}
						);
					}
				);
			}
		}
	);
};