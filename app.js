/**
 * Module dependencies.
 */

var express = require('express'),
	routes = require('./routes'),
	http = require('http'),
	path = require('path'),
	
	passport = require('passport'),
	TwitterStrategy = require('passport-twitter').Strategy;

var app = express();

app.configure(function(){
	app.set('port', process.env.PORT || 3000);
	app.set('views', __dirname + '/views');
	app.set('view engine', 'ejs');
	app.use(express.favicon());
	app.use(express.logger('dev'));
	app.use(express.bodyParser());
	app.use(express.methodOverride());
	app.use(express.cookieParser('cu-bedu-goqu-whu-gheb-jo-xe'));
	app.use(express.session());
	app.use(passport.initialize());
	app.use(passport.session());
	app.use(app.router);
	app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function(){
	app.use(express.errorHandler());
});

app.get('/', routes.index);


// Twitter Auth
passport.serializeUser(function(user, done) {
	done(null, user);
});
passport.deserializeUser(function(obj, done) {
	done(null, obj);
});
passport.use(
	new TwitterStrategy(
		{
			consumerKey: '4zb5zQ28nAsi93GYPWgHJw',
			consumerSecret: '73Qn3DQem1SS9jY5VFHB47OGAmcC5LG6SA1AsdkwnY',
			callbackURL: 'http://localhost:3000/auth/twitter/callback'
		},
		function(token, tokenSecret, profile, done) {
			// Assemble user object. Only use it for browser session, so not storing.
			var user = {
				token: token,
				tokenSecret: tokenSecret, 
				profile: profile
			};
			
			// Callback done
			console.log('done', user);
			done(null, user);
		}
	)
);

// Redirect the user to Twitter for authentication.	   When complete, Twitter
// will redirect the user back to the application at /auth/twitter/callback
app.get('/auth/twitter', passport.authenticate('twitter'));

// Twitter will redirect the user to this URL after approval. Finish the
// authentication process by attempting to obtain an access token. If
// access was granted, the user will be logged in. Otherwise,
// authentication has failed.
app.get('/auth/twitter/callback', 
	passport.authenticate('twitter',
		{
			successRedirect: '/twitter-share',
			failureRedirect: '/'
		}
	)
);

app.get('/twitter-share', routes.twitter_share);
app.post('/twitter-share', routes.twitter_publish);



http.createServer(app).listen(app.get('port'), function(){
	console.log("Express server listening on port " + app.get('port'));
});
